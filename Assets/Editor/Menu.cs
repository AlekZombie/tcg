﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public static class CardGameMenu {

    [MenuItem("Tools/CardGame/Create/Asset Card")]
    public static void AssetCard()
    {
        var ex = ScriptableObject.CreateInstance<AssetCard>();
        AssetDatabase.CreateAsset(ex, AssetDatabase.GenerateUniqueAssetPath("Assets/Resources/Asset/Card.asset"));
        AssetDatabase.SaveAssets();
    }

}