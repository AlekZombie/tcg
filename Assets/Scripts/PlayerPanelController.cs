﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerPanelController : MonoBehaviour {

    Player player;

    public Text PlayerNameDisplay;
    public Text PlayerLvlDisplay;
    public Text PlayerGoldDisplay;

    void Awake()
    {
        player = GetComponent<Player>();
    }
	// Update is called once per frame
	void Update () {
        PlayerNameDisplay.text = string.Format("{0}",player.nickname);
        PlayerLvlDisplay.text = string.Format("Lv: {0}", player.level);
        PlayerGoldDisplay.text = string.Format("{0}", player.Money);
	}
}
