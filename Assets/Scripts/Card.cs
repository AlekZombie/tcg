﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Card : MonoBehaviour
{
    [Header("Asset Card")]
    [SerializeField]
    AssetCard _assetcard;

    [Header("Text Component References")]
    [SerializeField]
    Text CardTitle;
    [SerializeField]
    Text CardDescription;
    [SerializeField]
    Text CardHealth;
    [SerializeField]
    Text CardAttack;
    [SerializeField]
    Text CardDefense;
    [SerializeField]
    Text CardCost;
    [SerializeField]
    Text CardLvl;
    [Header("Img Component References")]
    [SerializeField]
    Image Front;
    [SerializeField]
    Image Art;
    [SerializeField]
    Material Back;
    [SerializeField]
    Image Attack;
    [SerializeField]
    Image Defense;
    [SerializeField]
    Image Cost;
    [SerializeField]
    Image Health;
    [SerializeField]
    Image Level;
    [SerializeField]
    Image Quality;

    public AssetCard Assetcard
    {
        set { _assetcard = value; }
        get { return _assetcard; }
    }

    void FixedUpdate()
    {
        if (transform.parent.name.Equals("Deck") | transform.parent.name.Equals("Hand Two"))
        {
            if (Attack != null)
                Attack.gameObject.SetActive(false);
            if (Defense != null)
                Defense.gameObject.SetActive(false);
            if (Health != null)
                Health.gameObject.SetActive(false);
            if (Quality != null)
                Quality.gameObject.SetActive(false);
            if (Art != null)
                Art.gameObject.SetActive(false);
        }
        else
        {
            if (Attack != null)
                Attack.gameObject.SetActive(true);
            if (Defense != null)
                Defense.gameObject.SetActive(true);
            if (Health != null)
                Health.gameObject.SetActive(true);
            if (Quality != null)
                Quality.gameObject.SetActive(true);
            if (Art != null)
                Art.gameObject.SetActive(false);

        }
    }

    void Start()
    {
        if (Assetcard != null)
        {
            Setup(Assetcard);
        }
    }

    public void Setup(AssetCard assetcard)
    {
        Assetcard = assetcard;

        CardTitle.text = assetcard.Name;

        if (assetcard.Type == Types.TypeOfCards.Minion)
        {
            if (CardHealth != null)
                CardHealth.text = assetcard.Health.ToString();
            if (CardAttack != null)
                CardAttack.text = assetcard.Attack.ToString();
            if (CardDefense != null)
                CardDefense.text = assetcard.Defense.ToString();
        }
        else
        {
            CardHealth.gameObject.SetActive(false);
            CardAttack.gameObject.SetActive(false);
            CardDefense.gameObject.SetActive(false);
        }

        //if (Quality != null)
        //{
        //    switch (assetcard.Quality)
        //    {
        //        case Types.QualityOfCard.Normal:
        //            Quality.sprite = Resources.Load<Sprite>("ogN");
        //            break;
        //        case Types.QualityOfCard.Rare:
        //            Quality.sprite = Resources.Load<Sprite>("ogR");
        //            break;
        //        case Types.QualityOfCard.Lengendary:
        //            Quality.sprite = Resources.Load<Sprite>("ogL");
        //            break;
        //        case Types.QualityOfCard.Epic:
        //            break;
        //    }
        //}

    }
}
