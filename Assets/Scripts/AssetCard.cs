﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AssetCard : ScriptableObject
{
    #region Fields
    [SerializeField]
    int _attack;
    [SerializeField]
    int _cost;
    [SerializeField]
    int _defense;
    [SerializeField]
    string _description;
    [SerializeField]
    int _health;
    [SerializeField]
    string _name;
    [SerializeField]
    int _level;
    [SerializeField]
    Types.TypeOfCards _type;
    [SerializeField]
    Types.QualityOfCard _quality;

    public int Attack
    {
        get { return _attack; }
    }

    public int Cost
    {
        get { return _cost; }
    }

    public int Defense
    {
        get { return _defense; }
    }

    public string Description
    {
        get { return _description; }
    }

    public int Health
    {
        get { return _health; }
    }

    public string Name
    {
        get { return _name; }
    }

    public int Level
    {
        get { return _level; }
    }

    public Types.TypeOfCards Type
    {
        get { return _type; }
    }

    public Types.QualityOfCard Quality
    {
        get { return _quality; }
    }
    #endregion
    


}
