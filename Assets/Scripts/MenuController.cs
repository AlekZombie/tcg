﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class MenuController : MonoBehaviour {

    public GameObject ActualContextMenu;

    public List<GameObject> ContextMenus;

    public List<GameObject> Buttons;

	void Awake () {
        foreach (GameObject btn in Buttons)
        {
            btn.GetComponent<Button>().onClick.AddListener(()=> { ChangeContextMenu(btn.GetComponent<Button>()); });
        }
	}
	
	// Update is called once per frame
	void Update () {    
		
	}

    void ChangeContextMenu(Button btn)
    {
        for (int i = 0; i < Buttons.Count; i++)
        {
            if (Buttons[i].name.Equals(btn.gameObject.name)) {
                if (!ActualContextMenu.name.Equals(ContextMenus[i].name))
                {
                    foreach (Transform child in ActualContextMenu.transform)
                    {
                        child.gameObject.SetActive(false);
                    }
                    ActualContextMenu.transform.DOLocalMoveX(1000, 1);
                    ActualContextMenu = ContextMenus[i];
                    ActualContextMenu.transform.DOLocalMoveX(85, 1, true).OnComplete(()=> {
                        foreach (Transform child in ActualContextMenu.transform)
                        {
                            child.gameObject.SetActive(true);
                        }
                    });
                    
                }
            }
        }
    }
}
