﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class HoverView : MonoBehaviour
{

    [SerializeField]
    private GameObject _hoverObject;
    [SerializeField]
    private GameObject _frontToDisable;
    [SerializeField]
    private bool _isDragged;

    public bool IsDragged { get { return _isDragged; } set { _isDragged = value; } }

    public GameObject HoverObject { get { return _hoverObject; } }

    public GameObject FronToDisable { get { return _frontToDisable; } }

    void Start()
    {
        HoverObject.GetComponent<Card>().Assetcard = gameObject.transform.GetComponentInParent<Card>().Assetcard;
    }

    void OnMouseEnter()
    {
        if (!IsDragged)
        {
            EnablingAndDisabling(false, true);
            HoverObject.transform.DOLocalMove(new Vector3(0, 80, 0), 0)
                .OnPlay(() => { HoverObject.transform.DOScale(2, 1); });
        }
    }

    void OnMouseExit()
    {
        if (!IsDragged)
        {
            EnablingAndDisabling(true, false);
        }

    }

    public void EnablingAndDisabling(bool statusforfront, bool statusforhover)
    {
        FronToDisable.SetActive(statusforfront);
        HoverObject.SetActive(statusforhover);
    }

}
