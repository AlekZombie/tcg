﻿using System;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;
using System.Linq;

public class Player : MonoBehaviour
{
    [Header("Player Info")]
    public string nickname;
    public Types.PlayerPosition Position;
    private int _money = 1000;
    public int level = 1;

    public int Money
    {
        get { return _money; }
        set { if (value > 0) _money = value; }
    }


    [Header("Hand Info")]
    public GameObject Hand;

    [Header("Deck Info")]
    public Deck deck;
    public Transform deckPosition;

    void Awake()
    {
        if (SceneManager.GetActiveScene().name.Equals("Game")){
            deck = new Deck();
            SettingsCardOnField();
        }
    }

    public void SettingsCardOnField()
    {
        foreach (AssetCard ac in deck.Cards)
        {
            GameObject CardFromDeck = Instantiate(Resources.Load<GameObject>("Prefabs/Card"));
            CardFromDeck.GetComponent<Card>().Setup(ac);
            CardFromDeck.transform.SetParent(deckPosition, false);
        }
    }

    public void DrawCardFromDeck()
    {
        if (deck.Cards.Count != 0)
        {
            Transform card = deckPosition.GetChild(0);
            card.DOLocalMove(new Vector3(315, 80, 0), 1).OnComplete(() =>
            {
                switch (Position)
                {
                    case Types.PlayerPosition.Down: //Rotate the card for the player
                        card.DOLocalRotate(new Vector3(0, 180, 0), 1, RotateMode.LocalAxisAdd).OnComplete(() =>
                        {
                            card.SetParent(Hand.transform);
                        });
                        break;
                    case Types.PlayerPosition.Up:
                        card.SetParent(Hand.transform);
                        break;
                }
            });
        }
    }

}