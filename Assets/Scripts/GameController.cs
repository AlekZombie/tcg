﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class GameController : MonoBehaviour
{
    public static GameController Instance { get; set; }

    private enum Phase { EnemyTurn, YourTurn};

    [Header("Player's Scripts")]
    public Player PlayerOne;
    public Player PlayerTwo;

    [Header("GameController Components")]
    [SerializeField]
    private Phase phase;
    [SerializeField]
    private int PlayerOneTurns;
    [SerializeField]
    private int PlayerTwoTurns;
    [SerializeField]
    private Text PlayerTurnDisplay;
    [SerializeField]
    private Button EndTurn;

    #region OwnMethods

    void EnablaingAndDisablingDraggable(Draggable d, Transform child, bool status)
    {
        if (d != null)
        {
            child.gameObject.GetComponent<Draggable>().enabled = status;
        }
    }

    void EnablingAndDisablingPlayers(Player playeronturn, Player secondplayer)
    {
        if(playeronturn.Hand.transform.childCount != 0)
        {
            foreach(Transform child in playeronturn.Hand.transform)
            {
                EnablaingAndDisablingDraggable(child.gameObject.GetComponent<Draggable>(), child, true);
            }
        }

        if(secondplayer.Hand.transform.childCount != 0)
        {
            foreach(Transform child in secondplayer.Hand.transform)
            {
                EnablaingAndDisablingDraggable(child.gameObject.GetComponent<Draggable>(), child, false);
            }
        }
    }

    void DoChangeTurn(Phase phase, string turn, Player ToDraw)
    {
        EndTurn.enabled = false;
        this.phase = phase;
        PlayerTurnDisplay.text = turn;
        PlayerTurnDisplay.gameObject.SetActive(true);
        PlayerTurnDisplay.transform.DOScale(3, 3)
                .OnComplete(() => {
                    PlayerTurnDisplay.transform.DOScale(0, 1.5f)
                        .OnComplete(() => {
                            ToDraw.DrawCardFromDeck();
                            PlayerTurnDisplay.gameObject.SetActive(false);
                            EndTurn.enabled = true;
                        });
                });
    }

    public void ChangeTurn()
    {
        if (phase == Phase.YourTurn)
            DoChangeTurn(Phase.EnemyTurn, "Enemy Turn", PlayerTwo);
        else
            DoChangeTurn(Phase.YourTurn, "Your Turn", PlayerOne);
    }

    #endregion

    #region UnityMethods
    void Awake()
    {
        Instance = this;
    }

    void FixedUpdate()
    {
        if (phase == Phase.YourTurn)
        {
            EnablingAndDisablingPlayers(PlayerOne, PlayerTwo);
        }
        else
        {
            EnablingAndDisablingPlayers(PlayerTwo, PlayerOne);
        }
    }

    #endregion

}
