﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;


public class Draggable : MonoBehaviour
{
    public bool UserPointerDisplacement = true;

    private bool dragging = false;

    public Vector3 pointerDisplacement = Vector3.zero;

    public float zDisplacement;

    public Transform parentToReturnTo = null;
    GameObject placeholder = null;

    void CreatePlaceHolder()
    {
        placeholder = new GameObject();
        placeholder.transform.SetParent(transform.parent);
        LayoutElement le = placeholder.AddComponent<LayoutElement>();
        le.preferredWidth = GetComponent<LayoutElement>().preferredWidth;
        le.preferredHeight = GetComponent<LayoutElement>().preferredHeight;
        le.flexibleHeight = 0;
        le.flexibleWidth = 0;
    }

    void Update()
    {
        if (dragging)
        {
            Vector3 mousePos = MouseInWorldCoords();
            transform.position = new Vector3(mousePos.x - pointerDisplacement.x, mousePos.y - pointerDisplacement.y, 1);
        }
    }

    void OnMouseDown()
    {
        if (this.enabled != false)
        {
            dragging = true;

            GetComponent<HoverView>().EnablingAndDisabling(true, false);
            GetComponent<HoverView>().IsDragged = true;

            zDisplacement = -Camera.main.transform.position.z + transform.position.z;
            if (UserPointerDisplacement)
                pointerDisplacement = -transform.position + MouseInWorldCoords();
            else
                pointerDisplacement = Vector3.zero;


            CreatePlaceHolder();

            placeholder.transform.SetSiblingIndex(transform.GetSiblingIndex());

            parentToReturnTo = transform.parent;
            transform.SetParent(transform.parent.parent);

            GetComponent<CanvasGroup>().blocksRaycasts = false;

        }
    }

    private Vector3 MouseInWorldCoords()
    {
        var screenMousePos = Input.mousePosition;
        screenMousePos.z = zDisplacement;
        return Camera.main.ScreenToWorldPoint(screenMousePos);
    }

    void OnMouseUp()
    {
        if (this.enabled != false)
        {

            if (dragging)
            {
                dragging = false;
            }
            transform.SetParent(parentToReturnTo);
            transform.SetSiblingIndex(placeholder.transform.GetSiblingIndex());
            GetComponent<CanvasGroup>().blocksRaycasts = true;


            Destroy(placeholder);
            GetComponent<HoverView>().IsDragged = false;

        }
    }
}
