﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class GlowImage : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    Image image;
    bool isOver = false;

    void Awake() {
        image = GetComponent<Image>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        image.sprite = Resources.Load<Sprite>(string.Format("{0}-over",image.sprite.name));
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        image.sprite = Resources.Load<Sprite>(string.Format("{0}", image.sprite.name.Remove(image.sprite.name.Length-5)));

    }
}
