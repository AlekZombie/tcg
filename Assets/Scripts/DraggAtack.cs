﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DraggAtack : MonoBehaviour {

    public LineRenderer lineRenderer;

    public Transform origin;
    public Transform destination;

    public bool Attacking;

	// Use this for initialization
	void Start () {
        origin = transform;
        Attacking = false;
        lineRenderer.startWidth = .45f;
	}

    void Update()
    {

    }

    private Vector3 MouseInWorldCoords()
    {
        var screenMousePos = Input.mousePosition;
        return Camera.main.ScreenToWorldPoint(screenMousePos);
    }

    void OnMouseDown()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Attacking = true;
            GetComponent<HoverView>().EnablingAndDisabling(true, false);
            GetComponent<HoverView>().IsDragged = true;
        }
    }

    void OnMouseUp()
    {
        Attacking = false;
        GetComponent<HoverView>().IsDragged = false;
    }
}
