﻿public class Types
{
    public enum TypeOfCards
    {
        Minion,
        Spell
    }

    public enum QualityOfCard
    {
        Normal,
        Rare,
        Epic,
        Lengendary
    }

    public enum PlayerPosition
    {
        Up,
        Down
    }

    public enum Scenes
    {
        InGame,
        ShopCamera,
        MainCamera,
        GaleryCamera,
    }
}