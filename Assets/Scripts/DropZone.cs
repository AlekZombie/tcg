﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

public class DropZone : MonoBehaviour
{

    void OnTriggerEnter(Collider other)
    {
        Draggable d = other.gameObject.GetComponent<Draggable>();

        if (d != null)
        {
            d.parentToReturnTo = transform;
            switch (gameObject.name)
            {
                case "Hand":
                    d.transform.localRotation = new Quaternion(0, 0, 180, 0);       
                    break;
                case "Table":
                    d.transform.localRotation = new Quaternion(0, 0, 0, 0);
                    break;
                case "Table Two":
                    d.transform.localRotation = new Quaternion(0, 0, 180, 0);
                    break;
                case "Hand Two":
                    d.transform.localRotation = new Quaternion(180, 0, 0, 0);
                    break;
            }
        }
    }
}
