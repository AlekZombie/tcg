﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Deck{

    public List<AssetCard> Cards;

    public Deck()
    {
        Cards = new List<AssetCard>();
        Cards.Add(Resources.Load<AssetCard>("Asset/Minion 1"));
        Cards.Add(Resources.Load<AssetCard>("Asset/Minion 2"));
        Cards.Add(Resources.Load<AssetCard>("Asset/Minion 3"));
        System.Random rand = new System.Random();
        var List = Cards.OrderBy(c => rand.Next()).Select(c => c).ToList();
        Cards = List;
    }

}
